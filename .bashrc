#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export HISTCONTROL=ignoredups:ignorespace
export HISTIGNORE='p:n:history:ls:e'
export HISTSIZE=10000

alias ls='ls --color=auto'
alias p='python'
alias n='ncmpcpp'
alias e='mount | grep -E "sdb|sdc"'

#PS1='[\u@\h \W]\$ '
PS1='\h standing by => '

